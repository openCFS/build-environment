openCFS build-environment
=========================

A collection of docker containers as a common CFS build environment on Linux.
Containers defined in the project are hosted on [docker-hub](https://hub.docker.com/orgs/opencfs), and used in the CI/CD pipeline of openCFS.

Docker
-------------

### Basics

To create a container, go to the directory and run
```
docker build -t <some-tag-name> .
```

You can then run the container by
```
docker run -ti <some-tag-name> bash
```
To have root privileges inside the docker container (e.g. for testing package installation), use argument `-u root`.

To pull existing images from docker hub use
```
docker pull docker.io/opencfs/fedora41
```
where the leading domain name `docker.io/` can be omitted (for docker, which by default pulls from docker.io).

### Advanced topics

We use [multi-stage builds](lhttps://docs.docker.com/build/building/multi-stage/) to re-use common stuff between different images.
This is useful for a common MKL installation (see `mkl/min`), which can be re-used across all images.

Using images for development
----------------------------

To mount the CFS source to `/cfs` in the container use the argument `-v ~/my/cfs/path:/cfs`.
It makes sense to store builds outside the container (otherwise they are destroyed when the container is quit).
Use the source directory from above or provide another host dir.
For systems with SELinux enabled you can pass the `--security-opt label=disable` option to be able to mount stuff from the host user's home directory.

For __rootless podman__ containers, you should switch to root in the container (pass `--user root` option) to keep your uid.
Thereby _root_ in the container is _your user_ on the host (for details see the [rootless podman tutorial](https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md)).
In this way, you can access mounts with correct user permissions on the host.
An example using rootless podman:
```
podman run --security-opt label=disable --user root -v /opt/cfs/centos6-gcc7:/build:rw -v ~/openCFS/CFS:/cfs:ro -ti docker.io/opencfs/centos6-gcc7:latest bash
```

An even better workflow is to use [toolbox](https://containertoolbx.org/).
This way you get the build environment defined in the container directly on your machine in your home directory.
Some of our containers are already ready to be used with toolbox (e.g. `fedora41`), older ones might not work.
First create a _toolbox_ based on the image
```
toolbox create -i docker.io/opencfs/fedora41 my-cfs-devel-toolbox
```
Then enter the toolbox with
```
toolbox enter my-cfs-devel-toolbox
```
and you should have all required tools available.
Toolbox takes care of permissions and mounting the host home directory into the container.
Thus, you can keep working as usual.

Container Images on a Container Registry
----------------------------------------

We use [docker hub](https://hub.docker.com/) to publish our containers.
They are collected in the organization [openCFS](https://hub.docker.com/orgs/opencfs).
The containers can be built via GitLab-CI and are then published automatically (username/password for login are set via *tokens* made available as *CI/CD variables*).
See `.gitlab-ci.yml` for details.

One can also push containers to docker-hub manually. Before you can push containers you need to [authenicate to the container registry](https://docs.gitlab.com/12.10/ee/user/packages/container_registry/#authenticating-to-the-gitlab-container-registry) e.g. by
```
docker login [OPTIONS] [SERVER]
```
Additionally, the images are pushed to the [integrated container registry on gitlab](https://gitlab.com/openCFS/build-environment/container_registry) accessible via `registry.gitlab.com/opencfs/build-environment`.

Maintenance Notes
-----------------

The naming convention for containers is `<BaseSystem>-<Variant>`, where `<BaseSystem>` denotes common stuff for all variants.
* use a directory for each base system and place the docker file in there
* create sub-diretories for variants, and use the `FROM <BaseSystem>` in the docker file
* define the build/test jobs in the pipeline

The [CI pipeline of this repo](.gitlab-ci.yml) will build the defined docker containers and push them to [docker-hub openCFS](https://hub.docker.com/orgs/opencfs) with the following concept:
* There is one build job per `<BaseSystem>` to save on up/downloads (variants are built with existing base-systems)
* Each image is pushed as `<BaseSystem>-<Variant>:$CI_COMMIT_REF_NAME`, i.e. the docker tag (after `:`) corresponds to the branch/tag name (`$CI_COMMIT_REF_NAME`)
* The protected default branch of the git repo is called __latest__, which automatically corresponds to the default docker tag
* Creating tags in the git repo creates corresponding docker tags by running the pipeline
* We use tags in the format `YYYY-MM[-DD]` to make the CFS pipelines repeatable (by explicitly using the immutable `image:tag` container)
  Upon tag-creation the pipeline runs and deploys to docker-hub

Each git-tag created for the default branch should create a docker-tag that can be used in the build pipeline.


CFS build pipeline
-------------------

We use the docker containers in this repo as runners in the CFS build pipeline.
All Linux builds and tests currently use docker containers, although shell-executor based runners would also work. 
Generally we have different containers for _builds_, _test_, and _release builds_.
Testing on a different container ensures the build is portable, and the systems used for the release build habe old standard c-libs.
Pipeline jobs should request immutable containers by explicitly specifying `image:tag` (as compared to `image` only which defaults to `image:latest`).
Thereby, old pipelines keep running and cannot be broken by new developments in the build-environment repo.

To check the CFS pipeline with a new image use the pipeline variables `BUILD_ENVIRONMENT_REGISTRY` and `BUILD_ENVIRONMENT_TAG`.
You can pre-populate the variables using links:
* [CFS master pipeline with `latest` images from gitlab-registry](https://gitlab.com/openCFS/cfs/-/pipelines/new?ref=master&var[BUILD_ENVIRONMENT_REGISTRY]=registry.gitlab.com/opencfs/build-environment&var[BUILD_ENVIRONMENT_TAG]=latest)
* [CFS master pipeline with `latest` images from docker.io (default)](https://gitlab.com/openCFS/cfs/-/pipelines/new?ref=master&var[BUILD_ENVIRONMENT_TAG]=latest)

Of course one can set `BUILD_ENVIRONMENT_TAG` to the branch one is working on.

